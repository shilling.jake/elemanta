FROM openjdk:8-alpine

COPY target/uberjar/elementa.jar /elementa/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/elementa/app.jar"]
