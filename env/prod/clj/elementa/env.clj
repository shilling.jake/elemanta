(ns elementa.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[elementa started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[elementa has shut down successfully]=-"))
   :middleware identity})
