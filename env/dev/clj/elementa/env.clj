(ns elementa.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [elementa.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[elementa started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[elementa has shut down successfully]=-"))
   :middleware wrap-dev})
